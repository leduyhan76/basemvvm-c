//
//  TestProjTests.swift
//  TestProjTests
//
//  Created by Han Le on 7/13/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import XCTest
@testable import TestProj

class TestProjTests: XCTestCase {
    
    var viewModel = UserViewModel()
    
    func testDataLoaded() {
        var num = 0
        let expectation = self.expectation(description: "numOfData")
        
        viewModel.users?.observe({ (users) in
            guard let users = users else {return}
            num = users.value.count
            expectation.fulfill()
        })

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(num, 30)
    }

    
    func testFollowerAndFollowing() {
        var follower = 0
        var following = 0
        
        let expectation = self.expectation(description: "checkFollowerAndFollowing")
        
        viewModel.userDetail?.observe({ (users) in
            guard let user = users else {return}
            follower = user.followers ?? 0
            following = user.following ?? 0
            expectation.fulfill()
        })

        waitForExpectations(timeout: 2, handler: nil)
        XCTAssert(follower > 0 && following > 0)
    }
    
}
