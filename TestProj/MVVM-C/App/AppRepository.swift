//
//  ApiService.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

class AppRepository: Singleton {
    
    lazy var apiClient: ApiClient = get()
    lazy var appCache: AppCache = get()
    
    required init(){
        
    }
    
    func getUser() throws -> Users {
        let users = try Users.from(try apiClient.get(path: "users") as Any)
        appCache.setUsers(users: users)
        return users
    }
    
    
    func getUser(_ index:Int,_ size:Int) throws -> Users {
        if let u = appCache.getUsers() {
            return u
        }
        let users = try Users.from(try apiClient.get(path: "users") as Any)
        appCache.setUsers(users: users)
        return users
    }
    
    func getUserDetail(_ id: String) throws -> UserDetail? {
        guard let json = try apiClient.get(path: "users/" + String(id) + "?\(Date().timeIntervalSince1970)") as? [String : Any] else {
            throw Throwable.CanNotGetUser
        }
        let detail = try UserDetail.from(json)
        
        if detail.id == nil{
            throw Throwable.CanNotGetUser
        }
        
        appCache.setUserDetail(detail)
        
        return detail
    }
}
