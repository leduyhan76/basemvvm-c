//
//  AppCache.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation



class AppCache: Singleton {
  
    fileprivate let userDefaults: UserDefaults = UserDefaults.standard
    
    private var mUsers : Users?
    
    required init() {
        
    }
    
    func getUserDetail(id:Int) -> UserDetail? {
        guard let json = userDefaults.object(forKey: "USERID_\(id)") as? [String:Any] else {
            return nil
        }
        guard let model: UserDetail = try? UserDetail.from(json) else {
            return nil
        }
        return model
    }
    
    func getUsers() -> Users? {
        if mUsers == nil {
            guard let json = userDefaults.object(forKey: UserDefaultKey.UserData.rawValue) as? [[String:Any]] else {
                return nil
            }
            mUsers = try? Users.from(json)
        }
        
        return mUsers
    }
    
    func setUsers(users: Users?) {
        mUsers = users
//        guard let json_ = mUsers?.value as? [[String:Any]] else {
//            return
//        }
//
//        self.userDefaults.set(json_, forKey: UserDefaultKey.UserData.rawValue)
//        self.userDefaults.synchronize()
    }
    
    func setUserDetail(_ user:UserDetail?)  {
        userDefaults.set(user?.toDictionary(), forKey: "USERID_\(user?.login ?? "0")")
        userDefaults.synchronize()
    }
    
    
    func loadJson<T: Codable>(fileName: String) -> T? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try JSONDecoder().decode(T.self, from: data)
            } catch let error {
                // handle error
                print(error)
            }
        }
        return nil
    }


}
