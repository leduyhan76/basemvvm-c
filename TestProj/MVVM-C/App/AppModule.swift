//
//  AppModule.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import Foundation

let navigatorModule = module {
    factory {
        UserNavigator() as UserRoute
    }
}

let newsModule = module {
    factory {
        NewsNavigator() as NewsRoute
    }
}

let networkModule = module {
    single {
        AlamofireClient(host:"https://api.github.com") as ApiClient
    }
}
