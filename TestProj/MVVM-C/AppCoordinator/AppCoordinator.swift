//
//  LoginNavigator.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import Foundation

protocol UserRoute {
    
    func start()
    
    func list()
    
    func detail(id:Int)
}

protocol NewsRoute {
    func switchToNews()
}

class AppCoordinator: NavCoordinator {
    lazy var user: UserRoute = create()
    lazy var news: NewsRoute = create()
}
