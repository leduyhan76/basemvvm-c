//
//  Navigator.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class UserNavigator: Navigator, UserRoute {
    
    init() {
        super.init(AppStoryboard.User.instance)
    }
    
    func start(){
        start(LoginViewController.self)
    }
    
    func list(){
        startNavigation(ListUserViewController.self)
    }
    
    func detail(id: Int) {
        with(UserDetailViewController.self).apply{ it in
            it.id = id
        }.push()
    }

}


class NewsNavigator: Navigator, NewsRoute {
    
    init() {
        super.init(AppStoryboard.News.instance)
    }
        
    func switchToNews() {
        with(NewsViewController.self).push()
    }
}


