//
//  AppStoryBoard.swift
//  TestProj
//
//  Created by Est Rouge on 10/23/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryboard : String {
    
    case User
    case News

    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).className
        return (instance.instantiateViewController(withIdentifier: storyboardID) as? T) ?? T()
    }
    
    func initViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}
