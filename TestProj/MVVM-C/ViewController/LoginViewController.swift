//
//  LoginViewController.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController<LoginViewModel> {
    
    @IBOutlet weak var tfUserID: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnlang: UIButton!
    
    lazy var coordinator:AppCoordinator = get()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.loginSuccess.observe(self){[unowned self] (users) in
            guard let user = users else {return}
            if user.id != nil {
                self.coordinator.user.list()
            }
        }
        
        viewModel.loading.observe(self) {[unowned self] (it) in
            if it ?? false{
                self.loading()
            }else{
                self.stop()
            }
        }
    }
    
    override func localizeUI() {
        btnLogin.setTitle(localized(multiLanguage.kLogin), for: .normal)
        tfUserID.placeholder = localized(multiLanguage.kPlaceHolderID)
        btnlang.setTitle(LanguageService.service.current_language.uppercased(), for: .normal)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        viewModel.login.setValue(tfUserID?.text)
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        let changeToLanguage = btnlang.titleLabel?.text == "VI" ? SystemLanguage.en : SystemLanguage.vi
        LanguageService.service.switchToLanguage(changeToLanguage)
        
    }
}
