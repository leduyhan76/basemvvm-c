//
//  ListUserViewController.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import ESPullToRefresh


class ListUserViewController: BaseViewController<UserViewModel>{
    
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var users = Users()
    
    lazy var coordinator:AppCoordinator = get()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        viewModel.loadMultipleApi.observe(self){[unowned self] (arg0) in
//            let (users, _) = arg0!
//            self.users = users
//            self.tableView.reloadData()
//        }
        
        viewModel.users.observe(self){[unowned self] (users) in
            guard let users = users else {return}
            self.users = users
            self.tableView.reloadData()
        }

        viewModel.loading.observe(self){[unowned self] (isLoading) in
            if isLoading ?? false {
                print("Loading True")
                self.refreshControl.beginRefreshing()
                self.loading()
            }else{
                print("Loading FALSE")
                self.refreshControl.endRefreshing()
                self.stop()
            }
        }
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.setupMenu()
        self.tableView.refreshControl = self.refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }
    
    @objc func refreshData() {
        print("Refresh")
        viewModel.refresh.call()
    }
}


extension ListUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListUserTableViewCell.className, for: indexPath) as! ListUserTableViewCell
        cell.updateUI(data: users.value[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.onDetail?(users.value[indexPath.row].id)
        if indexPath.row == 0 {
            self.coordinator.currentController = self
            self.coordinator.news.switchToNews()
        }else{
            self.coordinator.user.detail(id: users.value[indexPath.row].id!)
        }
    }
    
}
