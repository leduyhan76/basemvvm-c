//
//  UserDetailViewController.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import ESPullToRefresh

class UserDetailViewController: BaseViewController<UserDetailViewModel> {
    
    @IBOutlet weak var tableView: UITableView!
    
    let refreshControl = UIRefreshControl()
    var id: Int? {
        didSet {
            idUserTest = id ?? 0
        }
    }
    var user:UserDetail?
    var appEvent: AppEvent = get()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.userDetail.observe(self){[weak self] (users) in
            print("Loading User")
            guard let user = users else {return}
            self?.user = user
            self?.tableView.reloadData()
        }
        viewModel.loading.observe(self){[weak self] (isLoading) in
           
            if isLoading ?? false {
              print("Loading True")
                self?.refreshControl.beginRefreshing()
                self?.loading()
            }else{
                print("Loading False")
                self?.refreshControl.endRefreshing()
                self?.stop()
            }
        }
        
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        self.setTitle("Profile")
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.refreshControl = self.refreshControl
        
        viewModel.userId.setValue(self.id)
    }
    
    @objc func refreshData() {
        viewModel.userId.refresh()
        appEvent.reloadUserList.call()
    }
    
}


extension UserDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch  indexPath.row{
        case 0:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: InfoDetailTableViewCell.className, for: indexPath) as! InfoDetailTableViewCell
            cell1.updateUI(data: user)
            return cell1
        case 1:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: DetailAboutTableViewCell.className, for: indexPath) as! DetailAboutTableViewCell
            cell2.updateUI(data: user)
            return cell2
        default:
            let cell3 = tableView.dequeueReusableCell(withIdentifier: DetailStatsTableViewCell.className, for: indexPath) as! DetailStatsTableViewCell
            cell3.updateUI(data: user)
            return cell3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
