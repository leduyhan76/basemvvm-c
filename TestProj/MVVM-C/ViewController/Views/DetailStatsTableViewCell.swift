//
//  DetailStatsTableViewCell.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class DetailStatsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbNumOfFollowing: UILabel!
    @IBOutlet weak var lbNumOfFollowers: UILabel!
    @IBOutlet weak var lbNumOfPubl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(data: UserDetail?) {
        guard let `data` = data else {
            return
        }
        lbNumOfPubl.text = "\(data.public_gists ?? 0)"
        lbNumOfFollowers.text = "\(data.followers ?? 0)"
        lbNumOfFollowing.text = "\(data.following ?? 0)"

    }

}
