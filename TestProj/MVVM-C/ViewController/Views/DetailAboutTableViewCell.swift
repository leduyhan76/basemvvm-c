//
//  DetailAboutTableViewCell.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class DetailAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var lbAbout: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(data: UserDetail?) {
        guard let `data` = data else {
            return
        }
        lbAbout.text = data.bio
    }

}
