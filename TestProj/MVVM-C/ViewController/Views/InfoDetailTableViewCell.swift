//
//  InfoDetailTableViewCell.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class InfoDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbLink: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(data: UserDetail?) {
        guard let `data` = data else {
            return
        }
    imgAvatar.kfImageURL(data.avatar_url ?? "")
        lbName.text = data.login
        lbLink.text = data.location
    }


}
