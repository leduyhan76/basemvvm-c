//
//  ListUserTableViewCell.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class ListUserTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbLink: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(data: User) {
        imgAvatar.kfImageURL(data.avatarUrl ?? "")
        lbName.text = data.login
        lbLink.text = data.htmlUrl
    }

}
