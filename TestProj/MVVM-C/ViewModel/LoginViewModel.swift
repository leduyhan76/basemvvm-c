//
//  LoginViewModel.swift
//  TestProj
//
//  Created by Quang Phan on 10/19/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class LoginViewModel: BaseViewModel {
    lazy var appRepo :AppRepository = get()
    var login = LiveData<String>()
    
//    lazy var loginSuccess:LiveData<UserDetail> = login.doNext{[unowned self] (next,it:String?) in
//        let param=it ?? ""
//        do{
//            self.loading.postValue(true)
//            next.postValue(try self.appRepo.getUserDetail(param))
//            self.loading.postValue(false)
//        }catch let e{
//            self.error.postValue(e.localizedDescription)
//        }
//
//    }
    required init() {
        super.init()
    }
    
    lazy var loginSuccess = login.async(self){[unowned self] it in
        try self.appRepo.getUserDetail(it ?? "")
    }
}
