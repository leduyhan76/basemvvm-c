//
//  UserViewModel.swift
//  TestProj
//
//  Created by Quang Phan on 10/19/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class UserViewModel: BaseViewModel {
    
    lazy var appRepo: AppRepository = get()
    lazy var appCache : AppCache = get()
    lazy var users = LiveData<Users>().loadOnDisk{[weak self] () in
        self?.appCache.getUsers()
    }
    .toMediator()
    .or(refresh.async(self){[unowned self] it in
        try self.appRepo.getUser()
    })
    
//    lazy var loadMultipleApi:LiveData<(Users,UserDetail)> = refresh.async(self){ it in
//        let a = async{ try self.appRepo.getUser()}
//        let b = async{ try self.appRepo.getUserDetail("1")}
//        let result = (try a.await(),try b.await())
//        print("Load refresh success")
//        return (result as! (Users, UserDetail))
//    }
    
    required init() {
        super.init()
        
        self.refresh.call()
    }
    
    deinit {
        
        print("deinit viewmodel")
        
    }
}
