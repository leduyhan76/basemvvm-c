//
//  UserDetailViewModel.swift
//  TestProj
//
//  Created by Quang Phan on 10/18/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class UserDetailViewModel: BaseViewModel {
    
    lazy var appRepo:AppRepository = get()
    lazy var appCache:AppCache = get()
    
    var userId = LiveData<Int>()
    private var test = LiveData<String>()
    
    lazy var userDetail: MediatorLiveData<UserDetail> = self.userId.map {[unowned self] it in
        print("Get user detail from cache ")
        return self.appCache.getUserDetail(id: it!)
    }
//    .or(userId.async(self){[unowned self] it in
//         try self.appRepo.getUserDetail("\(it!)")
//    })
    .filter { it in
        it != nil
    }
    
    required init() {
        super.init()
        
        userId.async(self){[unowned self] it in
            try self.appRepo.getUserDetail("\(it!)")
        }.notify(to: userDetail)
    }
    
    deinit {
        print("deinit UserDetailViewModel")
    }
    
}
