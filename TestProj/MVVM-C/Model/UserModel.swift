//
//  UserModel.swift
//  WareHouse
//
//  Created by TVT on 11/7/17.
//  Copyright © 2017 TVT25. All rights reserved.
//
import Foundation


struct BaseUserResponse: Codable {
    let data: [User]?
    init(from decoder: Decoder) throws {
        data = try decoder.decodeIfPresent("data")
    }
}


struct User: Codable {
    
    public var avatarUrl : String?
    public var eventsUrl : String?
    public var followersUrl : String?
    public var followingUrl : String?
    public var gistsUrl : String?
    public var gravatarId : String?
    public var htmlUrl : String?
    public var id : Int?
    public var login : String?
    public var nodeId : String?
    public var organizationsUrl : String?
    public var receivedEventsUrl : String?
    public var reposUrl : String?
    public var siteAdmin : Bool?
    public var starredUrl : String?
    public var subscriptionsUrl : String?
    public var type : String?
    public var url : String?
    
    init(from decoder: Decoder) throws {
        avatarUrl = try decoder.decodeIfPresent("avatar_url")
        eventsUrl = try decoder.decodeIfPresent("events_url")
        followersUrl = try decoder.decodeIfPresent("followers_url")
        followingUrl = try decoder.decodeIfPresent("following_url")
        gistsUrl = try decoder.decodeIfPresent("gists_url")
        gravatarId = try decoder.decodeIfPresent("gravatar_id")
        htmlUrl = try decoder.decodeIfPresent("html_url")
        id = try decoder.decodeIfPresent("id")
        login = try decoder.decodeIfPresent("login")
        nodeId = try decoder.decodeIfPresent("node_id")
        organizationsUrl = try decoder.decodeIfPresent("organizations_url")
        receivedEventsUrl = try decoder.decodeIfPresent("received_events_url")
        reposUrl = try decoder.decodeIfPresent("repos_url")
        siteAdmin = try decoder.decodeIfPresent("site_admin")
        starredUrl = try decoder.decodeIfPresent("starred_url")
        subscriptionsUrl = try decoder.decodeIfPresent("subscriptions_url")
        type = try decoder.decodeIfPresent("type")
        url = try decoder.decodeIfPresent("url")
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if avatarUrl != nil{
            dictionary["avatar_url"] = avatarUrl
        }
        if eventsUrl != nil{
            dictionary["events_url"] = eventsUrl
        }
        if followersUrl != nil{
            dictionary["followers_url"] = followersUrl
        }
        if followingUrl != nil{
            dictionary["following_url"] = followingUrl
        }
        if gistsUrl != nil{
            dictionary["gists_url"] = gistsUrl
        }
        if gravatarId != nil{
            dictionary["gravatar_id"] = gravatarId
        }
        if htmlUrl != nil{
            dictionary["html_url"] = htmlUrl
        }
        if id != nil{
            dictionary["id"] = id
        }
        if login != nil{
            dictionary["login"] = login
        }
        if nodeId != nil{
            dictionary["node_id"] = nodeId
        }
        if organizationsUrl != nil{
            dictionary["organizations_url"] = organizationsUrl
        }
        if receivedEventsUrl != nil{
            dictionary["received_events_url"] = receivedEventsUrl
        }
        if reposUrl != nil{
            dictionary["repos_url"] = reposUrl
        }
        if siteAdmin != nil{
            dictionary["site_admin"] = siteAdmin
        }
        if starredUrl != nil{
            dictionary["starred_url"] = starredUrl
        }
        if subscriptionsUrl != nil{
            dictionary["subscriptions_url"] = subscriptionsUrl
        }
        if type != nil{
            dictionary["type"] = type
        }
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }
    
    static func from(_ json :[String:Any]) throws -> User{
        return try JSONDecoder().decode(User.self, from: json.jsonData()!)
    }
    
}


struct UserDetail : Codable {
    
    let avatar_url : String?
    let bio : String?
    let blog : String?
    let company : String?
    let created_at : String?
    let email : String?
    let events_url : String?
    let followers : Int?
    let followers_url : String?
    let following : Int?
    let following_url : String?
    let gists_url : String?
    let gravatar_id : String?
    let hireable : Bool?
    let html_url : String?
    let id : Int?
    let location : String?
    let login : String?
    let name : String?
    let node_id : String?
    let organizations_url : String?
    let public_gists : Int?
    let public_repos : Int?
    let received_events_url : String?
    let repos_url : String?
    let site_admin : Bool?
    let starred_url : String?
    let subscriptions_url : String?
    let type : String?
    let updated_at : String?
    let url : String?
    
    
    init(from decoder: Decoder) throws {
        avatar_url = try decoder.decodeIfPresent("avatar_url")
        bio = try decoder.decodeIfPresent("bio")
        blog = try decoder.decodeIfPresent("blog")
        company = try decoder.decodeIfPresent("company")
        created_at = try decoder.decodeIfPresent("created_at")
        email = try decoder.decodeIfPresent("email")
        events_url = try decoder.decodeIfPresent("events_url")
        followers = try decoder.decodeIfPresent("followers")
        followers_url = try decoder.decodeIfPresent("followers_url")
        following = try decoder.decodeIfPresent("following")
        following_url = try decoder.decodeIfPresent("following_url")
        gists_url = try decoder.decodeIfPresent("gists_url")
        gravatar_id = try decoder.decodeIfPresent("gravatar_id")
        hireable = try decoder.decodeIfPresent("hireable")
        html_url = try decoder.decodeIfPresent("html_url")
        id = try decoder.decodeIfPresent("id")
        location = try decoder.decodeIfPresent("location")
        login = try decoder.decodeIfPresent("login")
        name = try decoder.decodeIfPresent("name")
        node_id = try decoder.decodeIfPresent("node_id")
        organizations_url = try decoder.decodeIfPresent("organizations_url")
        public_gists = try decoder.decodeIfPresent("public_gists")
        public_repos = try decoder.decodeIfPresent("public_repos")
        received_events_url = try decoder.decodeIfPresent("received_events_url")
        repos_url = try decoder.decodeIfPresent("repos_url")
        site_admin = try decoder.decodeIfPresent("site_admin")
        starred_url = try decoder.decodeIfPresent("starred_url")
        subscriptions_url = try decoder.decodeIfPresent("subscriptions_url")
        type = try decoder.decodeIfPresent("type")
        updated_at = try decoder.decodeIfPresent("updated_at")
        url = try decoder.decodeIfPresent("url")
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if avatar_url != nil{
            dictionary["avatar_url"] = avatar_url
        }
        if bio != nil{
            dictionary["bio"] = bio
        }
        if blog != nil{
            dictionary["blog"] = blog
        }
        if company != nil{
            dictionary["company"] = company
        }
        if created_at != nil{
            dictionary["created_at"] = created_at
        }
        if email != nil{
            dictionary["email"] = email
        }
        if events_url != nil{
            dictionary["events_url"] = events_url
        }
        if followers != nil{
            dictionary["followers"] = followers
        }
        if followers_url != nil{
            dictionary["followers_url"] = followers_url
        }
        if following != nil{
            dictionary["following"] = following
        }
        if following_url != nil{
            dictionary["following_url"] = following_url
        }
        if gists_url != nil{
            dictionary["gists_url"] = gists_url
        }
        if gravatar_id != nil{
            dictionary["gravatar_id"] = gravatar_id
        }
        if hireable != nil{
            dictionary["hireable"] = hireable
        }
        if html_url != nil{
            dictionary["html_url"] = html_url
        }
        if id != nil{
            dictionary["id"] = id
        }
        if location != nil{
            dictionary["location"] = location
        }
        if login != nil{
            dictionary["login"] = login
        }
        if name != nil{
            dictionary["name"] = name
        }
        if node_id != nil{
            dictionary["node_id"] = node_id
        }
        if organizations_url != nil{
            dictionary["organizations_url"] = organizations_url
        }
        if public_gists != nil{
            dictionary["public_gists"] = public_gists
        }
        if public_repos != nil{
            dictionary["public_repos"] = public_repos
        }
        if received_events_url != nil{
            dictionary["received_events_url"] = received_events_url
        }
        if repos_url != nil{
            dictionary["repos_url"] = repos_url
        }
        if site_admin != nil{
            dictionary["site_admin"] = site_admin
        }
        if starred_url != nil{
            dictionary["starred_url"] = starred_url
        }
        if subscriptions_url != nil{
            dictionary["subscriptions_url"] = subscriptions_url
        }
        if type != nil{
            dictionary["type"] = type
        }
        if updated_at != nil{
            dictionary["updated_at"] = updated_at
        }
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }
    
    static func from(_ json :[String:Any]) throws -> UserDetail{
        return try JSONDecoder().decode(UserDetail.self, from: json.jsonData()!)
    }
    
}

class Users {
    var value = [User]()
    
    init() {
    }
    
    func plus(_ users:Users)->Users{
        let result = Users()
        users.value.forEach { it in
            result.value.append(it)
        }
        self.value.forEach{it in
            result.value.append(it)
        }
        return result
    }
    
    static func from (_ json_:Any) throws -> Users{
        let users = Users()
        guard let array = json_ as? [[String : Any]] else {
            return users
        }
        for item in array {
            users.value.append(try User.from(item))
        }
        return users
    }
}
