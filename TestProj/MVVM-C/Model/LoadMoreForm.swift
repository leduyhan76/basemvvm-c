//
//  LoadMoreForm.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

struct LoadMoreForm {
    var pageSize:Int
    var pageIndex :Int
    init(size:Int, index:Int) {
        self.pageSize = size
        self.pageIndex = index
    }
}
