//
//  BaseModel.swift
//  WareHouse
//
//  Created by TVT on 11/7/17.
//  Copyright © 2017 TVT25. All rights reserved.
//

import Foundation


struct BaseResponse: Codable {
    
    let message : String?
    let code : Int?
    let result: Bool?
    
    
    init(from decoder: Decoder) throws {
        result = try decoder.decodeIfPresent("result")
        message = try decoder.decodeIfPresent("message")
        code = try decoder.decodeIfPresent("code")
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["massage"] = message
        }
        if result != nil{
            dictionary["result"] = result
        }
        return dictionary
    }
}
