//
//  BaseViewModel.swift
//  TestProj
//
//  Created by Han Le on 7/14/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

class BaseViewModel: Injectable, Disposer {
    
    var error = LiveData<Throwable>()
    var refresh = LiveData<Any>()
    var loading = LiveData<Bool>()
    
    private var mDisposables = [Disposable]()
    
    required init() {}
    
    open func onClear()  {
        cleanUp()
        Mirror(reflecting: self).children.forEach{it in
            (it.value as AnyObject as? Disposable)?.release()
        }
    }
    
    func push(_ disposable: Disposable) {
        mDisposables.append(disposable)
    }
    
    func cleanUp() {
        mDisposables.forEach{it in
            it.release()
        }
        mDisposables.removeAll()
    }
    
}
