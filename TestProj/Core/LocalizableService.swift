//
//  LocalizableService.swift
//  iNails
//
//  Created by ThanhPham on 5/11/18.
//  Copyright © 2018 TVT25. All rights reserved.
//

import Foundation


enum SystemLanguage: String {
    case vn
    case en
    case vi
    case zh_hans = "zh-Hans"
    case ja
    case ko
}


let DynamicLanguageServiceDidDetectLanguageSwitchNotificationKey = "DynamicLanguageServiceDidDetectLanguageSwitchNotificationKey"


func localized(_ key: multiLanguage) -> String {
    return LanguageService.service.dynamicLocalizedString(key.rawValue)
}

class LanguageService: Injectable {
    
    fileprivate let userDefaults: UserDefaults = UserDefaults.standard
    
    private struct Defaults {
        static let keyCurrentLanguage = "KeyCurrentLanguage"
    }
    
    
    var current_language: String {
        get {
            return userDefaults.string(forKey: UserDefaultKey.kLanguage.rawValue) ?? "vi"
        }
        set {
            userDefaults.set(newValue, forKey: UserDefaultKey.kLanguage.rawValue)
            userDefaults.synchronize()
        }
    }
    
    static let service:LanguageService = LanguageService()
    
    var languageCode: String {
        get {
            return language.rawValue
        }
    }
    
    var defaultLanguageForLearning:SystemLanguage {
        get {
            var language: SystemLanguage = .vi
            if current_language == language.rawValue {
                language = .en
            }
            return language
        }
    }
    
    func switchToLanguage(_ lang:SystemLanguage) {
        language = lang
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: DynamicLanguageServiceDidDetectLanguageSwitchNotificationKey), object: nil)
    }
    
    func clearLanguages() {
        current_language = "vi"
    }
    
    private var localeBundle:Bundle?
    
    fileprivate var language: SystemLanguage = SystemLanguage.en {
        didSet {
            let currentLanguage = language
            current_language = currentLanguage.rawValue
            setLocaleWithLanguage(currentLanguage.rawValue)
        }
    }
    
    
    required internal init() {
        prepareDefaultLocaleBundle()
    }
    
    //MARK: - Private
    
    fileprivate func dynamicLocalizedString(_ key: String) -> String {
        var localizedString = key
        if let bundle = localeBundle {
            localizedString = NSLocalizedString(key, bundle: bundle, comment: "")
        } else {
            localizedString = NSLocalizedString(key, comment: "")
        }
        return localizedString
    }
    
    private func prepareDefaultLocaleBundle() {
        
        updateCurrentLanguageWithName(current_language)
        
    }
    
    private func updateCurrentLanguageWithName(_ languageName: String) {
        if let lang = SystemLanguage(rawValue: languageName) {
            language = lang
        }
    }
    
    private func setLocaleWithLanguage(_ selectedLanguage: String) {
        if let pathSelected = Bundle.main.path(forResource: selectedLanguage, ofType: "lproj"),
            let bundleSelected = Bundle(path: pathSelected)  {
            localeBundle = bundleSelected
        } else if let pathDefault = Bundle.main.path(forResource: SystemLanguage.en.rawValue, ofType: "lproj"),
            let bundleDefault = Bundle(path: pathDefault) {
            localeBundle = bundleDefault
        }
    }
    
    func loadLanguage()  {
        switchToLanguage(SystemLanguage.init(rawValue: current_language)!)
    }
}

protocol Localizable {
    func localizeUI()
}
