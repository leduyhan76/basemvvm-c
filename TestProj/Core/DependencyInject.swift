import Foundation

protocol Beanable {
    func getInstance() -> AnyObject
}

protocol Injectable {
    init()
}

protocol Singleton : Injectable {}

class DIBean<T> : Beanable{
    var singleton:Bool
    var callback:()->T
    var value:AnyObject?
    
    init(_ singleton:Bool, _  callback: @escaping ()->T){
        self.singleton = singleton
        self.callback = callback
    }
    
    func getInstance() -> AnyObject {
        if !singleton {
            return newInstance()
        }
        if self.value == nil {
            self.value = newInstance()
        }
        return self.value!
    }
    
    func newInstance() -> AnyObject {
        return self.callback() as AnyObject
    }
}

class DIContext {
    static let instance = DIContext()
    
    var mCache = [String: Beanable]()
    
    private func stringOf<T>(_ type:T.Type)->String{
        return "\(type)"
    }
    
    func single<T>(_  callback: @escaping ()->T) {
        mCache[stringOf(T.self)] = DIBean<T>(true, callback)
    }
    
    func factory<T>(_  callback: @escaping ()->T){
        mCache[stringOf(T.self)] = DIBean<T>(false, callback)
    }
    
    func get<T : Injectable>() -> T{
        let key = stringOf(T.self)
        var bean = mCache[key]
        if bean == nil {
            bean = DIBean<T>(T.self is Singleton.Type){ T.init() }
            mCache[key] = bean
        }
        return bean!.getInstance() as! T
    }
    
    func get<T>() -> T {
        let key = stringOf(T.self)
        let bean = mCache[key]
        if bean == nil {
            fatalError("Class \(key) not found, please provide before get")
        }
        return bean!.getInstance() as! T
    }
}

class Module{
    private let load:()->Void
    init(_ load: @escaping ()->Void) {
        self.load = load
    }
    
    func provide(){
        self.load()
    }
}

func single<T>(_ callback: @escaping ()->T){
    DIContext.instance.single(callback)
}

func factory<T>(_ callback: @escaping ()->T){
    DIContext.instance.factory(callback)
}

func get<T>() -> T {
    return DIContext.instance.get()
}

func get<T:Injectable>() -> T {
    return DIContext.instance.get()
}

func module(_ function:@escaping ()->Void) -> Module{
    return Module(function)
}

func modules(_ modules: Module...){
    modules.forEach{it in
        it.provide()
    }
}

// Begin scope test

//class TestInjectable : Singleton {
//
//    lazy var user:User = get()
//
//    required init(){
//        print("Init Test")
//    }
//
//    func toString()->String{
//        return "Hello world \(user.toString())"
//    }
//}
//
//class User {
//    init() {
//         print("Init User")
//    }
//
//    func toString() -> String {
//        return "User"
//    }
//}
//
//class Cache{
//    var user:User
//    init(_ user:User) {
//        self.user = user
//        print("Init Cache")
//    }
//
//    func toString() -> String {
//        return "Cache \(user.toString())"
//    }
//}
//
//single { User() }
//single { Cache(get()) }
//
//class Hello{
//    lazy var text:Cache = get()
//    lazy var temp:User = get()
//    lazy var test:TestInjectable = get()
//}
//
//let hello = Hello()
//
//print(hello.text.toString())
//print(hello.temp.toString())
//print(hello.text.toString())
//print(hello.test.toString())
//
//let hello1 = Hello()
//print(hello1.text.toString())
//print(hello1.temp.toString())
//print(hello1.text.toString())
//print(hello1.test.toString())
