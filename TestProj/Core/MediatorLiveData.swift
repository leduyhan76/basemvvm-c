//
//  MediatorLiveData.swift
//  TestProj
//
//  Created by Han Le on 7/15/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

protocol Activable {
    func inactive()
    func active()
    func release()
}

private class Source<T>:Activable {
    private var mLastVersion = START_VERSION
    
    var observable: LiveData<T>?
    
    private var call: Observer<T>?
    
    init(_ observable: LiveData<T>,_  observer: Observer<T>) {
        self.observable = observable
        self.call = Observer<T> { it in
            if (observable.dataVersion != self.mLastVersion) {
                self.mLastVersion = observable.dataVersion
                observer.call(it)
            }
        }
    }
    
    func inactive() {
        observable?.removeObserver(call!)
    }
    
    func active() {
        observable?.observeForever(call!)
    }
    
    func release() {
        call = nil
        (observable as? MediatorLiveData)?.release()
        observable = nil
    }
}
class MediatorLiveData<T> : LiveData<T>,Disposable {
    
    private var mSources = [Int : Activable]()
    
    func addSource<V>(_ observable: LiveData<V>,_ function: Observer<V>) {
        let source = Source(observable, function)
        mSources[observable.hashValue] = source
        if (hasActive()) {source.active()}
    }
    
    func removeSource<V>(_ observable: LiveData<V>) {
        let source = mSources.removeValue(forKey: observable.hashValue)
        source?.inactive()
        source?.release()
    }
    
    override func onActive() {
        super.onActive()
        mSources.forEach {it in
            it.value.active()
        }
    }
    
    override func onInactive() {
        super.onInactive()
        mSources.forEach {it in
            it.value.inactive()
        }
    }
    
    func release() {
        mSources.forEach{(key,value) in
            value.release()
        }
        mSources.removeAll()
    }
}
