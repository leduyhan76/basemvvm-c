//
//  ApiClient.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class AlamofireClient : ApiClient {
    
    var host:String
    
    init(host:String) {
        self.host = host
    }
    
    func get( path:String,  params: [String:Any] = [:],  headers:[String:Any] = [:]) throws -> Any? {
        return try continuation({con in
            if !Connectivity.isConnectedToInternet() {
                return con.error(Throwable.NoNetwork)
            }
            let path_ = "\(self.host)/\(path)"
            print(path_) 
            
            Alamofire.SessionManager.default
                .request(path_, method: .get, parameters: params, encoding: URLEncoding.default, headers: (headers as! HTTPHeaders))
                .responseJSON(completionHandler: { (response) in
                    self.logIfNeeded(response: response)
                    switch response.result {
                    case .success:
                        guard let json = response.result.value as? [[String : Any]] else {
                            guard let jsonObj = response.result.value as? [String : Any] else {
                                return con.error(Throwable.NoNetwork)
                            }
                            con.resume(jsonObj)
                            return
                        }
                        con.resume(json)
                        
                    case .failure(let error):
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            return con.error(Throwable.NoNetwork)
                        } else if error._code == NSURLErrorTimedOut {
                            return con.error(Throwable.NoNetwork)
                        } else {
                            return con.error(Throwable.NoNetwork)
                        }
                    }
                })
        })
    }
    
    
    func post( url:String,  params: [String:Any] = [:],  headers:[String:Any] = [:]) throws -> Any? {
        return try continuation({con in
            
            if !Connectivity.isConnectedToInternet() {
                return con.error(Throwable.NoNetwork)
            }
            
            Alamofire.SessionManager.default
                .request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: (headers as! HTTPHeaders))
                .responseJSON(completionHandler: { (response) in
                    
                    self.logIfNeeded(response: response)
                    
                    switch response.result {
                    case .success:
                        
                        guard let json = response.result.value as? [[String : Any]] else {
                            guard let jsonObj = response.result.value as? [String : Any] else {
                                return con.error(Throwable.NoNetwork)
                            }
                            con.resume(jsonObj)
                            return
                        }
                        
                        con.resume(json)
                        
                    case .failure(let error):
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            return con.error(Throwable.NoNetwork)
                        } else if error._code == NSURLErrorTimedOut {
                            return con.error(Throwable.NoNetwork)
                        } else {
                            return con.error(Throwable.NoNetwork)
                        }
                    }
                })
        })
    }
    
    
    func upload(_ path: String, _ params: [String: Any]?,_ headers:[String:String] = [:], data: [Dictionary<String, Any>]?) throws -> Any?{
        return try continuation({con in
            if !Connectivity.isConnectedToInternet() {
                return con.error(Throwable.NoNetwork)
            }
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 30
            manager.upload(multipartFormData:{ multipartFormData in
                
                if let parameters = params {
                    for (key, value) in parameters {
                        if let encode = "\(value)".data(using: String.Encoding.utf8) {
                            multipartFormData.append(encode, withName: key)
                        }
                    }
                }
                if let dataImages = data {
                    for dict in dataImages {
                        guard let data: Data = dict["value"] as? Data, let key: String = dict["key"] as? String else {return}
                        multipartFormData.append(data, withName: key, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    }
                }
            }, usingThreshold:UInt64.init(),
               to:path,
               method:.post,
               headers:headers,
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("RESPONE: \(response.result.value ?? [:])")
                        
                        guard let json = response.result.value as? [[String : Any]] else {
                            guard let jsonObj = response.result.value as? [String : Any] else {
                                return con.error(Throwable.NoNetwork)
                            }
                            con.resume(jsonObj)
                            return
                        }
                        
                        con.resume(json)
                    }
                case .failure(let error):
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        return con.error(Throwable.NoNetwork)
                    } else if error._code == NSURLErrorTimedOut {
                        return con.error(Throwable.NoNetwork)
                    } else {
                        return con.error(Throwable.NoNetwork)
                    }
                }
            })
        })
    }
    
    private func logIfNeeded(response:DataResponse<Any>){
        if let data = response.result.value{
            #if DEBUG
            print(data)
            #else
            #endif
        }
    }
}
