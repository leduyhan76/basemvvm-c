//
//  ApiClient.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

protocol ApiClient  {
    func get(path:String, params: [String:Any], headers:[String:Any]) throws -> Any?
    
    func post(url:String, params: [String:Any], headers:[String:Any]) throws -> Any?
    
    func upload(path:String, params: [String:Any], headers:[String:String], data: [Dictionary<String, Any>]) throws -> Any?

}

extension ApiClient {
    func get(path:String, params: [String:Any] = [:], headers:[String:Any] = [:]) throws -> Any?{
        return try get(path:path,params:params,headers:headers)
    }
    
    func post(path:String, params: [String:Any] = [:], headers:[String:Any] = [:]) throws -> Any?{
        return try post(path:path,params:params,headers:headers)
    }
    
    func upload(path:String, params: [String:Any] = [:], headers:[String:String] = [:], data: [Dictionary<String, Any>] = [[:]]) throws -> Any?{
        return try upload(path:path,params:params,headers:headers, data: data)
    }

}
