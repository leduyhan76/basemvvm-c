//
//  Lifecycle.swift
//  TestProj
//
//  Created by Han Le on 7/15/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

protocol LifecycleOwner{
    func getLifecycle() -> Lifecycle
}

struct LifecycleObserver {
    private let uuid = UUID()
    
    static func == (lhs: LifecycleObserver, rhs: LifecycleObserver) -> Bool {
        lhs.uuid == rhs.uuid
    }
    
    var onChanged:()->Void
    init(callback:@escaping ()->Void) {
        self.onChanged = callback
    }
}

class Lifecycle {
    
    static let DID_LOAD = 0
    
    static let WILL_APPEAR = 1
    
    static let DID_APPEAR = 2
    
    static let WILL_DISAPPEARED = 3
    
    static let DID_DISAPPEARED = 4
    
    static let DID_UNLOAD = 5
    
    var currentState = -1
    
    var callbacks = [Int : [LifecycleObserver]]()
    
    func isAtLeast(_ state:Int) -> Bool {
        return currentState - state >= 0
    }
    
    func dispatch(event:Int){
        self.currentState = event
        notifyStateChanged(event: event)
    }
    
    func registry(event:Int,callback:LifecycleObserver)->Lifecycle{
        if  self.callbacks[event] == nil{
            self.callbacks[event] = [LifecycleObserver]()
        }
        
        self.callbacks[event]?.append(callback)
        if self.currentState == event {
            callback.onChanged()
        }
        return self
    }
    
    func remove(_ event:Int,_ callback:LifecycleObserver) {
        var eventCallbacks = callbacks[event]
        if eventCallbacks != nil{
            for i in 0..<eventCallbacks!.count {
                if eventCallbacks![i] == callback {
                    eventCallbacks?.remove(at: i)
                    return
                }
            }
        }
    }
    
    private func notifyStateChanged(event:Int) {
        self.callbacks[event]?.forEach { callback in
            let call = callback as LifecycleObserver
            call.onChanged()
        }
    }
}
