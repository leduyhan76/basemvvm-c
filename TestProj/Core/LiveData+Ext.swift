//
//  LiveData+Ext.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

extension LiveData{
    
    func map<V>(_ function: @escaping (T?) throws -> V?) -> MediatorLiveData<V> {
        let next = MediatorLiveData<V>()
        next.addSource(self,Observer<T>{ it in
            do{
                next.setValue(try function(it))
            }catch let e {
                print(e.localizedDescription)
            }
        })
        return next
    }
    
    func doNext<V>(_ function: @escaping (LiveData<V>,T?) throws -> Void) -> MediatorLiveData<V> {
        let next = MediatorLiveData<V>()
        next.addSource(self,Observer<T>{it in
            DispatchQueue.global(qos: .background).async {
                do{
                    try function(next,it)
                }catch let e{
                    print(e.localizedDescription)
                }
            }
        })
        return next
    }
    
    func switchMap<V>(_ function: @escaping (T?) -> LiveData<V>) ->  MediatorLiveData<V> {
        let next = MediatorLiveData<V>()
        var oldSource :LiveData<V>?
        next.addSource(self,Observer<T>{ it in
            let newSource = function(it)
            if (oldSource == newSource) {return}
            if oldSource != nil{
                next.removeSource(oldSource!)
            }
            next.addSource(newSource, Observer<V>{data in
                next.setValue(data)
            })
            oldSource = newSource
        })
        return next
    }
    
    func async<V>(_ viewModel:BaseViewModel,_ function:@escaping (T?) throws ->V?) -> MediatorLiveData<V> {
        let next = MediatorLiveData<V>()
        next.addSource(self,Observer<T>{[weak viewModel] it in
            viewModel?.loading.postValue(true)
            DispatchQueue.global(qos: .background).async {
                do{
                    let data = try function(it)
                    next.postValue(data)
                }catch let e {
                    if e is Throwable{
                        viewModel?.error.postValue((e as! Throwable))
                    }else{
                        viewModel?.error.postValue(e as? Throwable)
                    }
                }
                viewModel?.loading.postValue(false)
            }
        })
        return next
    }
    
    func loadOnDisk(_ callback:@escaping () throws ->T?) -> LiveData<T>{
        weak var me = self
        DispatchQueue.global(qos: .background).async {
            let data = try? callback()
            me?.postValue(data ?? nil)
        }
        return self
    }
    
    func notify(to: MediatorLiveData<T>){
        to.addSource(self,Observer{ it in
            to.setValue(it)
        })
    }
    
    func toMediator() -> MediatorLiveData<T>{
        let next = MediatorLiveData<T>()
        next.addSource(self,Observer{ it in
            next.setValue(it)
        })
        return next
    }
    
    func filter(_ condition: @escaping (T?)->Bool) -> MediatorLiveData<T>{
        let next = MediatorLiveData<T>()
        next.addSource(self,Observer{ it in
            if condition(it){
                next.setValue(it)
            }
        })
        return next
    }
}

extension MediatorLiveData{
    func or(_ source:LiveData<T>) -> MediatorLiveData<T>{
        addSource(source,Observer{ it in
            self.setValue(it)
        })
        return self
    }
}
