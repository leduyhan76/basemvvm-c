//
//  Navigator.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class Navigator {
    private var coordinator: NavCoordinator?
    private let storyBoard:UIStoryboard
    var currentViewController:UIViewController?
    
    init(_ storyBoard:UIStoryboard) {
        self.storyBoard = storyBoard
    }
    
    func set(_ coordinator:NavCoordinator){
        self.coordinator = coordinator
    }
    
    func with<T:UIViewController>(_ type:T.Type)-> Dispatcher<T> {
        return Dispatcher(controller(type), self)
    }
    
    func controller<T:UIViewController>(_ type:T.Type) -> T{
        let storyboardID = (type as UIViewController.Type).className
        let storyBoard = self.storyBoard
        return (storyBoard.instantiateViewController(withIdentifier: storyboardID) as? T) ?? T()
    }
    
    func start<T:UIViewController>(_ type:T.Type){
        let viewController = controller(type)
        makeRoot(viewController)
    }
    
    func startNavigation<T:UIViewController>(_ type:T.Type){
        let viewController = controller(type)
        makeRootNavigation(viewController)
    }
    
    private func makeRoot(_ viewController:UIViewController) {
        coordinator?.window?.rootViewController = viewController
        coordinator?.window?.makeKeyAndVisible()
        currentViewController = viewController
    }
    
    private func makeRootNavigation(_ viewController:UIViewController){
        if viewController is UINavigationController {
            makeRoot(viewController)
        } else {
            makeRoot(BaseNavigationController(rootViewController: viewController))
        }
    }
}

struct Dispatcher<T:UIViewController> {
    private let controller:T
    private let navigator:Navigator
    
    init(_ it:T,_ navigator:Navigator) {
        self.controller = it
        self.navigator = navigator
    }
    
    private func findNavController() -> UINavigationController {
        let currentViewController = navigator.currentViewController
        var navigate: UINavigationController
        
        if let navigationController = currentViewController as? UINavigationController {
            navigate = navigationController
        } else if let navigationController = currentViewController?.navigationController{
            navigate = navigationController
        } else if let navigationController =  self.topViewController()?.navigationController{
           navigate = navigationController
        }else{
            fatalError("Not found Navigation Controller \(currentViewController?.className ?? "Unknown")")
        }
        return navigate
    }
    
    private func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let navigationController = controller as? UINavigationController {
                return topViewController(controller: navigationController.visibleViewController)
            }
            if let tabController = controller as? UITabBarController {
                if let selected = tabController.selectedViewController {
                    return topViewController(controller: selected)
                }
            }
            if let presented = controller?.presentedViewController {
                return topViewController(controller: presented)
            }
            return controller
    }
    
    private func actualViewController(for viewController: UIViewController) -> UIViewController {
        if let navigationController = viewController as? UINavigationController {
            return navigationController.viewControllers.first!
        } else {
            return viewController
        }
    }
    
    func apply(_ function:(T)->Void) -> Dispatcher<T>{
        function(controller)
        return self
    }
    
    func push() {
        let navigate = findNavController()
        navigate.pushViewController(controller)
    }
        
    func pop() {
        let navigate = findNavController()
        navigate.popViewController(animated: true)
    }
    
    func popToRoot() {
        let navigate = findNavController()
        navigate.popToRootViewController(animated: true)
    }
    
    
    func present() {
        let navigate = findNavController()
        controller.modalPresentationStyle = .fullScreen
        navigate.present(controller, animated: true, completion: nil)
    }

    func dismiss() {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func popAndPush<T>(popupTo type:T.Type){
        fatalError("Not implement yet!")
    }
    
}
