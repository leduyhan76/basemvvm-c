//
//  NavController.swift
//  TestProj
//
//  Created by Quang Phan on 10/26/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import Foundation

class NavCoordinator {
    
    private var mCurrent:UIViewController?
    let window:UIWindow?
    
    var currentController:UIViewController? {
        get {
            guard let mCurrent = mCurrent  else {
                return NavCoordinator.topViewController()
            }
            return mCurrent
        }
        set(value){
            mCurrent = value
        }
    }
    
    required init(window:UIWindow?) {
        self.window = window
    }
    
    private static func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    func create<T>() -> T{
        let navigator:T = get()
        guard let navigatorSafe = navigator as? Navigator else {
            fatalError("\(T.self) should be Navigator")
        }
        navigatorSafe.set(self)
        return navigator
    }
}
