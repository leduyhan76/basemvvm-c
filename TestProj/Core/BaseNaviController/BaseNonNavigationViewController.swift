//
//  BaseNonNavigationViewController.swift
//  iKanBid
//
//  Created by ThanhPham on 5/29/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

class BaseNonNavigationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func localizeUI() {
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func goto(_ viewController: UIViewController ) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
