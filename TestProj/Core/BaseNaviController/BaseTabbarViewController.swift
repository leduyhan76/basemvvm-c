//
//  AdminTabbarViewController.swift
//  Soho
//
//  Created by ThanhPham on 4/12/19.
//  Copyright © 2019 KANTEK. All rights reserved.
//

import UIKit

class BaseTabbarViewController: UITabBarController, UITabBarControllerDelegate {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI() {
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = .white
        self.delegate = self
        if let tabBarItems = tabBar.items {
            for tabBarItem in tabBarItems
            {
                //                tabBarItem.title = ""
                //                tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                
                tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.darkGray], for: .normal)
                tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.green], for: .selected)
            }
        }
    }
    

}
