//
//  BaseNavigationController.swift
//  WareHouse
//
//  Created by TVT on 11/7/17.
//  Copyright © 2017 TVT25. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavUI()
    }
    func setupNavUI() {
        self.delegate = self
        // Changing the Font of Navigation Bar Title
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: "Helvetica Neue", size: 15)!, NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key : Any]
        
        self.navigationBar.barTintColor = .white
        
        // Changing the Color and Font of Back button
        UINavigationBar .appearance().tintColor = UIColor.black
        
        
        //         Hidden title Back in UIBarButtonItem
        UIBarButtonItem .appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for: UIBarMetrics.default)
        
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "ic_back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "ic_back")
        
    }
    
}
