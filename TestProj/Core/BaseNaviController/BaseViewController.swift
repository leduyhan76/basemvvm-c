//
//  BaseViewController.swift
//  TestProj
//
//  Created by Han Le on 7/12/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController<T:BaseViewModel>: UIViewController, LifecycleOwner, NVActivityIndicatorViewable {
    var navTitle: String = ""
    lazy var lifecycle = Lifecycle()
    lazy var viewModel:T = get()

    func getLifecycle() -> Lifecycle {
        return lifecycle
    }
    
    func setTitle(_ str: String) {
        self.navigationItem.title = str
    }
    
    func setupMenu() {
        let menu: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        menu.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        menu.addTarget(self, action: #selector(self.menuAction), for: .touchUpInside)
        let leftItem = UIBarButtonItem(customView: menu)
        self.navigationItem.leftBarButtonItem = leftItem
    }
    
    @objc func menuAction() {
        //print show notification
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getLifecycle().dispatch(event: Lifecycle.DID_LOAD)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.localizeUI), name: NSNotification.Name(rawValue:DynamicLanguageServiceDidDetectLanguageSwitchNotificationKey), object: nil)
        
        localizeUI()

        self.viewModel.error.observe(self){[unowned self] (e) in
            if e?.toString == Throwable.NoNetwork.toString {
                self.showAlert(title: localized(multiLanguage.kNotification), message: localized((e?.toString)!))
                return
            }
            guard let str = e?.toString else { return }
            self.view.makeToast(localized(str))
        }
    }
    
    
    @objc func localizeUI() {
        
    }
    
    
    func loading() {
        self.startAnimating(type: .ballClipRotateMultiple)
    }
    
    func stop() {
        self.stopAnimating()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getLifecycle().dispatch(event: Lifecycle.WILL_APPEAR)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getLifecycle().dispatch(event: Lifecycle.DID_APPEAR)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getLifecycle().dispatch(event: Lifecycle.WILL_DISAPPEARED)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        getLifecycle().dispatch(event: Lifecycle.DID_DISAPPEARED)
    }
    
    deinit {
        getLifecycle().dispatch(event: Lifecycle.DID_UNLOAD)
        viewModel.onClear()
        print("Deinit controller")
    }
    
}
