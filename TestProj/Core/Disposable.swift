//
//  Disposable.swift
//  TestProj
//
//  Created by Quang Phan on 10/19/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import UIKit

protocol Disposable {
    func release()
}

protocol Disposer {
    func push(_ disposable:Disposable)
    func cleanUp()
}
