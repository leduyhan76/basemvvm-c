//
//  LiveData.swift
//  TestProj
//
//  Created by Han Le on 7/15/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

let START_VERSION = 0

enum MyError : Error{
    case runtimeError(String)
}

class ObjectComparable : Hashable{
    
    private let uuid = UUID()
    static func == (lhs: ObjectComparable, rhs: ObjectComparable) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    var hashValue: Int {
        return self.uuid.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        return uuid.hash(into: &hasher)
    }
}

class Observer<T>{
    let id = UUID().hashValue
    var callback :((T?)->Void)?
    
    init(function: @escaping (T?)->Void) {
        self.callback = function
    }
    
    func call(_ value:T?) {
        self.callback?(value)
    }
}

class LiveData<T>:ObjectComparable {
    
    private var mActiveCount = 0
    private var mObservers = [Int : ObserverWrapper<T>]()
    var dataVersion = START_VERSION
    private var mValue :T?
    open  func onInactive() {
        
    }
    open func onActive() {
        
    }
    
    func getValue() -> T?  {
        return mValue
    }
    
    func setValue(_ value:T?) {
        mValue = value
        dataVersion+=1
        notifyIfNeeded(value)
    }
    
    func postValue(_ value:T?) {
        if (Thread.isMainThread) {
            self.setValue(value)
        } else {
            DispatchQueue.main.async {
                self.setValue(value)
            }
        }
    }
    
    func call() {
        setValue(nil)
    }
    
    func refresh() {
        setValue(mValue)
    }
    
    private func notifyIfNeeded(_ value: T?) {
        mObservers.forEach { it in
            it.value.notifyChangeIfNeeded(value, dataVersion)
        }
    }
    
    private func addObserver(_ function: Observer<T>, callback:()->ObserverWrapper<T>)  {
        if mObservers[function.id] != nil {
            return
        }
        
        let observer = callback()
        mObservers[function.id] = observer
        
        observer.onActiveListener = {it in
            let oldActiveCount = self.mActiveCount
            if it {
                self.mActiveCount+=1
            } else{
                self.mActiveCount-=1
            }
            if oldActiveCount == 0 && self.mActiveCount != 0 {
                self.onActive()
            }else if oldActiveCount != 0 && self.mActiveCount == 0 {
                self.onInactive()
            }
        }
        observer.onRemoveListener={
            self.mObservers.removeValue(forKey: function.id)
        }
        observer.active(true)
        observer.notifyChangeIfNeeded(mValue, dataVersion)
    }
    
    func observeForever(_ function: Observer<T>) {
        addObserver(function){
            return ForeverObserver(function)
        }
    }
    
    func observe(_ owner:LifecycleOwner, function:@escaping (T?) -> Void){
        let observer = Observer{it in
            function(it)
        }
        addObserver(observer){
            return LifeObserver(owner.getLifecycle(),observer)
        }
    }
    
    func hasActive() ->Bool {
        return mActiveCount > 0
    }
    
    func removeObserver(_ function:Observer<T>) {
        let observer = mObservers[function.id]
        
        if observer != nil {
            observer!.active(false)
            observer!.detach()
        }
    }
    
    func removeObservers() {
        
        for (index, _) in mObservers.enumerated() {
            mObservers[index]?.active(false)
            mObservers[index]?.detach()
        }
    }
}

private class ForeverObserver<T> : ObserverWrapper<T> {
    override init(_ function: Observer<T>) {
        super.init(function)
    }
    override func isActive() -> Bool {
        return true
    }
}

private class LifeObserver<T> : ObserverWrapper<T> {
    
    private var mUpdateValue : T?
    private var mUpdateVersion = 0
    
    private var mActive = false
    private var owner:Lifecycle?
    
    var onStart : LifecycleObserver?
    var onStop : LifecycleObserver?
    var onDestroy :LifecycleObserver?
    
    init(_ lifecycle: Lifecycle,_ function: Observer<T>) {
        super.init(function)
        self.owner = lifecycle
        
        self.onStart = LifecycleObserver{
            self.active(true)
            self.notifyChangeIfNeeded(self.mUpdateValue, self.mUpdateVersion)
        }
        
        self.onStop = LifecycleObserver{
            self.active(false)
        }
        
        self.onDestroy = LifecycleObserver{
            self.detach()
        }
        
        let _ = lifecycle
            .registry(event: Lifecycle.WILL_APPEAR,callback:self.onStart!)
            .registry(event: Lifecycle.DID_DISAPPEARED,callback:self.onStop!)
            .registry(event: Lifecycle.DID_UNLOAD, callback: self.onDestroy!)
    }
    
    override func isActive() -> Bool {
        return mActive
    }
    
    func shouldBeActive() -> Bool {
        return owner!.isAtLeast(Lifecycle.WILL_APPEAR)
    }
    
    override func active(_ active: Bool) {
        if mActive == active {
            return
        }
        if !shouldBeActive() && active {
            return
        }
        mActive = active
        super.active(active)
    }
    
    override func detach() {
        super.detach()
        owner!.remove(Lifecycle.WILL_APPEAR,self.onStart!)
        owner!.remove(Lifecycle.DID_DISAPPEARED,self.onStop!)
        owner!.remove(Lifecycle.DID_UNLOAD, self.onDestroy!)
        self.onStop = nil
        self.onStart = nil
        self.onDestroy = nil
        self.owner = nil
    }
    
    override func notifyChangeIfNeeded(_ value: T?, _ version: Int) {
        mUpdateValue = value
        mUpdateVersion = version
        if shouldBeActive(){
            super.notifyChangeIfNeeded(value, version)
        }
    }
    
}

private class ObserverWrapper<T>: ObjectComparable{
    
    private var mLastVersion = START_VERSION
    var onActiveListener: ((Bool) -> Void)?
    var onRemoveListener: (() -> Void)?
    
    private var observer : Observer<T>?
    
    init(_ function: Observer<T>) {
        self.observer = function
    }
    
    open func isActive()->Bool {
        return false
    }
    
    open func detach()  {
        onRemoveListener?()
    }
    
    open func active(_ active:Bool)  {
        onActiveListener?(active)
    }
    
    open func notifyChangeIfNeeded(_ value: T?,_ version: Int) {
        
        if !isActive() || mLastVersion == version{
            return
        }
        observer?.call(value)
        mLastVersion = version
        
    }
}
