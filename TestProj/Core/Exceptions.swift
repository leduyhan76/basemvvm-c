//
//  Exceptions.swift
//  TestProj
//
//  Created by Han Le on 7/22/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

enum Throwable : Error {
    case NoNetwork
    case RequestTimeout
    case Unknown
    case CastFail
    case CanNotGetUser
}

extension Throwable {
    public var toString: multiLanguage? {
        switch self {
        case .NoNetwork:
            return .kNoNetWork
        case .RequestTimeout:
            return .kRequestTimeout
        case .Unknown:
            return .kUnknown
        case .CastFail:
            return .kCastFail
        case .CanNotGetUser:
            return .kCanNotGetUser
        }
    }
}
//enum ApiError : Error {
//    typealias RawValue = <#type#>
//
//    case NoNetwork = "No network"
//    case InternalServer = "Interal server "
//    case Authentication = "Not authen "
//    case Timeout="Request timeout"
//    case Unkfail ="Unknown"
//    case CastFail="Cast fail"
//    case runtimeError(String)
//}
