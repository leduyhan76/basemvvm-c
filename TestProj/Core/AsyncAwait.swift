import Foundation


let STATE_INIT = -1
let STATE_VALUE = 0
let STATE_ERROR = 1

class Coroutine<T>{
  var error :Error?
  var value :T?
  var state:Int
  let waiter = DispatchGroup()
  
  init() {
    self.state = STATE_INIT
    waiter.enter()
  }

  func await() throws ->T?{
    if state == STATE_INIT {
        waiter.wait()
    }
    
    if state == STATE_ERROR {
        throw self.error!
    }
    
    return self.value
  }
}

class Future<T> : Coroutine<T>{
  init(_ callback: @escaping () throws ->T?){
    super.init()
    DispatchQueue.global(qos: .background).async {
      do {
        self.value = try callback()
        self.state = STATE_VALUE
      } catch let error {
        self.error = error
        self.state = STATE_ERROR
      }
      self.waiter.leave()
    }
  }
}

enum ContinuationError : Error{
    case CallOutOfState
}

class Continuation<T> : Coroutine<T> {
    
    func resume(_ value:T?){
        if self.state != STATE_INIT {
            self.error(ContinuationError.CallOutOfState)
            return
        }
        self.state = STATE_VALUE
        self.value = value
        waiter.leave()
    }
    
    func error(_ error:Error){
        if self.state != STATE_INIT{
            return
        }
        self.state = STATE_ERROR
        self.error = error
        waiter.leave()
    }
}

func async<T>(_ callback: @escaping () throws ->T?) -> Future<T>{
  return Future<T>(callback)
}

func continuation<T>(_ callback: @escaping (Continuation<T>) -> Void) throws ->T? {
    let cont = Continuation<T>() 
    callback(cont)
    return try cont.await()
}

//enum MyError : Error{
//    case TestRunningFailure
//}
//
//func testAsyncSuccess(){
//    let a:Future<String> = async {
//      return "Task a run in background"
//    }
//    let b:Future<String> = async {
//        "Task b run in background"
//    }
//    do {
//        let c = "\(try a.await()) and \(try b.await())"
//        print("\(c)")
//    } catch let error {
//        print(error)
//    }
//}
//
//func testAsynFail(){
//    let a:Future<String> = async {
//      return "Task a run in background"
//    }
//    let b:Future<String> = async {
//        throw MyError.TestRunningFailure
//        return "Task b run in background"
//    }
//    do {
//        let c = "\(try a.await()) and \(try b.await())"
//        print("\(c)")
//    } catch let error {
//        print(error)
//    }
//}
//
//func getName() throws -> String? {
//    return try continuation { con in
//        DispatchQueue.global(qos: .background).async {
//            con.resume("Get Name from background")
//        }
//    }
//}
//
//func getAge() throws -> String? {
//    return try continuation { con in
//        DispatchQueue.global(qos: .background).async {
//            con.resume("Get age from background")
//        }
//    }
//}
//
//func getAddress() throws -> String? {
//    return try continuation { con in
//        DispatchQueue.global(qos: .background).async {
//            con.error(MyError.TestRunningFailure)
//        }
//    }
//}
//
//func testContinuationSuccess() {
//    do{
//        let a = try getName()!
//        let b = try getAge()
//        print("\(a) and \(b)")
//    } catch let e {
//        print(e)
//    }
//}
//
//func testContinuationFail() {
//    do{
//        let a = try getName()!
//        let b = try getAge()!
//        let c = try getAddress()!
//        print("\(a) and \(b) and \(c)")
//    } catch let e {
//        print(e)
//    }
//}
//
//testAsyncSuccess()
