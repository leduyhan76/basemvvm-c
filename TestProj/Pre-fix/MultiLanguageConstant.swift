//
//  MultiLanguageConstant.swift
//  TestProj
//
//  Created by Est Rouge on 10/27/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation

enum multiLanguage: String {
    //MARK: login
    case kLogin = "kLogin"
    case kPlaceHolderID = "kPlaceHolderID"
    
    //MARK: Users
    case kCanNotGetUser = "kCanNotGetUser"
    
    //Error
    case kNoNetWork = "kNoNetWork"
    case kRequestTimeout = "kRequestTimeout"
    case kUnknown = "kUnknown"
    case kCastFail = "kCastFail"
    
    //Notification
    case kNotification = "kNotification"
}
