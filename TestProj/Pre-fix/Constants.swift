//
//  Constants.swift
//  English4Kids
//
//  Created by Dong Nguyen on 4/3/18.
//  Copyright © 2018 Dong Nguyen. All rights reserved.
//

import UIKit


//MARK: - INTERNET
var IS_CONNETED_INTERNET = true

//MARK: - SCREEN FRAME
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)
let SIZEHEIGHT_NAVI = 64
let SIZEHEIGHT_TABBAR = 49


let IS_IPAD: Bool = (UIDevice.current.userInterfaceIdiom == .pad)
let IS_IPHONE: Bool = (UIDevice.current.userInterfaceIdiom == .phone)
let IS_RETINA: Bool = (UIScreen.main.scale >= 2.0)

let IS_IPHONE_4S: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH < 568.0))
let IS_IPHONE_5: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 568.0))
let IS_IPHONE_6: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 667.0))
let IS_IPHONE_6P: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 736.0))
let IS_IPHONE_X: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 812.0))


//MARK: - APPLICATION
let SHARE_APPLICATION_DELEGATE = UIApplication.shared.delegate as! AppDelegate

//MARK:- USER DEFAULT
enum UserDefaultKey: String {
    case UserData = "UserModel"
    case kLanguage = "kLanguage"
}

//MARK: ORTHER
var idUserTest = 0


