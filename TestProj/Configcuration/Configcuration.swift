//
//  Configcuration.swift
//  TestProj
//
//  Created by Est Rouge on 10/23/19.
//  Copyright © 2019 TVT25. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift

class Configuration: Singleton {
     
    let lang: LanguageService = get()
    
    required init() {
        modules(navigatorModule,networkModule, newsModule)
    }
    
    func setup() {
        IQKeyboardManager.shared.enable = true
        lang.loadLanguage()
    }
    
    func setupAppearance(_ window: UIWindow?) {
        single {
            return AppCoordinator(window: UIWindow())
        }
        let coor:AppCoordinator = get()
        coor.user.start()
    }
    
}


